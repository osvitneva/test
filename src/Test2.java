import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test2 {
	public static void main(String[] args) throws Exception {

		String stringData = Files.lines(Paths.get("./test.txt")).collect(Collectors.joining());

		List<Integer> listOfIntegers = Arrays.stream(stringData.split(","))
				.map(Integer::valueOf).collect(Collectors.toList());
		System.out.println("Исходные данные:" + listOfIntegers);

		String ascResult = listOfIntegers.stream().sorted().map(Object::toString)
				.collect(Collectors.joining(","));


		String descResult = listOfIntegers.stream().sorted(Comparator.reverseOrder()).map(Object::toString)
				.collect(Collectors.joining(","));
		System.out.println(String.format("По возрастанию: %s", ascResult));
		System.out.println(String.format("По убыванию: %s", descResult));









	}
}



